NAME = zipalign
SOURCES = ZipAlign.cpp ZipEntry.cpp ZipFile.cpp

SOURCES := $(foreach source, $(SOURCES), tools/zipalign/$(source))
LDFLAGS += -Wl,-rpath=/usr/lib/$(DEB_HOST_MULTIARCH)/android \
           -lpthread -lzopfli -lz \
           -L/usr/lib/$(DEB_HOST_MULTIARCH)/android \
           -lutils -llog -landroidfw
CXX := clang++
CXXFLAGS := -c
CXXOBJECTS := $(SOURCES:.cpp=.o)

build: $(CXXOBJECTS)
	$(CXX) $^ -o $(NAME) $(LDFLAGS)

clean:
	$(RM) $(CXXOBJECTS) $(NAME)

$(CXXOBJECTS): %.o: %.cpp
	$(CXX) $< -o $@  $(CXXFLAGS) $(CPPFLAGS)